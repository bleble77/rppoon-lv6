﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product Toy = new Product("Toycar hotwheels", 14);
            Box box = new Box();
            box.AddProduct(Toy);
            box.AddProduct(new Product("Doll", 12));
            IAbstractIterator iterator = box.GetIterator();
            while (iterator.IsDone == false)
            {
                Console.WriteLine( iterator.Current.ToString());
   
                iterator.Next();
            }
        }
    }
}
